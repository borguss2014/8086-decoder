@echo off

if not exist .\build\ (mkdir .\build\)
pushd .\build

cl -diagnostics:column -nologo -Od -Zi -W4 -WX -wd4189 -wd4100 -D_CRT_SECURE_NO_WARNINGS ..\main.cpp /link -incremental:no

popd