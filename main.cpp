#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <cassert>

#include "main.h"

function char
GetNextByte(disassembler *Disassembler)
{
    char Result = 0;
    if(Disassembler->At < Disassembler->End)
    {
        Result = *(Disassembler->At++);
    }
    else
    {
        Disassembler->Error = Disassembler_NoBytesAvailable;
    }
    
    return(Result);
}

function b32
BytesAvailable(disassembler *Disasembler)
{
    return(Disasembler->At < Disasembler->End);
}

function b32
NoDisassemblerError(disassembler *Disassembler)
{
    return(Disassembler->Error == Disassembler_NoError ? true : false);
}

operation OperationsList[256];
u32 NextOperation = 0;

//function operation *
//PushOperation()
//{
//assert(NextOperation < ArrayCount(OperationsList));
//
//operation *OpSlot = &OperationsList[NextOperation++];
//fprintf(stderr, "ERROR: Cannot push operation. Operation list is full! \n");
//
//return(OpSlot);
//}

function char *
DecodeRegisterField(char Reg, b32 Word = 0)
{
    u32 Index = (u32)(((char)Word << 3) | Reg);
    char *RegName = Index < ArrayCount(RegTable) ? RegTable[Index] : "NA-error";
    return(RegName);
}

//function u32
//GetTotalSetBits(char Byte)
//{
//u32 Result = 0;
//
//for(u32 Index = 0; Index < 8; ++Index)
//{
//if(Byte & 1)
//{
//Result++;
//}
//
//Byte = Byte >> 1;
//}
//
//return(Result);
//}

function u32
GetBitLength(char Byte)
{
    u32 Result = 0;
    u32 TotalShifts = 7;
    u32 TotalBits = 8;
    for(u32 ShiftIndex = TotalShifts; ShiftIndex >= 0; --ShiftIndex)
    {
        b32 IsCurrentBitSet = (Byte >> ShiftIndex) & 1;
        if(IsCurrentBitSet)
        {
            break;
        }
        
        Result++;
    }
    
    Result = 8 - Result;
    return(Result);
}

function char
GetBitmask(u32 NumberOfBitsToSet)
{
    char Result = (1 << NumberOfBitsToSet) - 1;
    return(Result);
}

function bool
IsOpcode(char Byte, opcode Opcode)
{
    b32 Result = false;
    u32 RequiredShiftBits = 8 - GetBitLength(Opcode);
    char ComparisonByte = (Byte >> RequiredShiftBits) & GetBitmask(8 - RequiredShiftBits);
    if(ComparisonByte & Opcode)
    {
        Result = true;
    }
    return(Result);
}

function string
DetermineInstruction(disassembler *Disassembler, char CurrentByte)
{
    string Output = {};
    
    /*if(IsOpcode(CurrentByte, OpCode_MOV_ImmediateToRegMem))
    {
    }
    else if(IsOpcode(CurrentByte, OpCode_MOV_MemToAccum))
    {
        
    }
    else if(IsOpcode(CurrentByte, OpCode_MOV_AccumToMem))
    {
        
    }
    else if(IsOpcode(CurrentByte, OpCode_MOV_ImmediateToRegister))
    {
        
    }*/
    if(IsOpcode(CurrentByte, OpCode_MOV_RegMemToRegMem))
    {
        char Direction = (CurrentByte >> 1) & 1;
        char Word = CurrentByte & 1;
        
        CurrentByte = GetNextByte(Disassembler);
        if(NoDisassemblerError(Disassembler))
        {
            b32 FullRegister = Word;
            
            char Mod = (CurrentByte >> 6) & 0b11;
            char Reg = (CurrentByte >> 3) & 0b111;
            char RM = CurrentByte & 0b111;
            
            switch(Mod)
            {
                case Mode_RegisterToRegister:
                {
                    string RMString = GetString(DecodeRegisterField(RM, FullRegister));
                    string RegString = GetString(DecodeRegisterField(Reg, FullRegister));
                    
                    string Source = (Direction == 1) ? RMString : RegString;
                    string Destination = (Direction == 1) ? RegString : RMString;
                    
                    char OutputBuffer[MAX_PATH];
                    sprintf(OutputBuffer, "%s %.*s, %.*s", 
                            "mov", 
                            Source.Count, Source.Data,
                            Destination.Count, Destination.Data);
                    CopyString(&Output, StringLength(OutputBuffer), OutputBuffer);
                } break;
            }
        }
    }
    
    return(Output);
}

struct file
{
    char *Buffer;
    mmsz BufferSize;
};

function disassembler
InitializeDisassembler(char *Data, u32 Size)
{
    disassembler Result = {};
    Result.Source.Data = Result.At = Data;
    Result.Source.Count = Size;
    Result.End = Result.At + Result.Source.Count;
    return(Result);
}

s32
main(int argc, char **argv)
{
    FILE *File = fopen("single-register", "rb");
    if(File)
    {
        mmsz FileSize = GetFileSize(File);
        char *Buffer = (char *)malloc(FileSize);
        if(Buffer)
        {
            mmsz BytesRead = 0;
            file_error Error = ReadFile(File, Buffer, FileSize, &BytesRead);
            if(NoFileError(Error))
            {
                disassembler Disassembler = InitializeDisassembler(Buffer, (u32)FileSize);
                while(BytesAvailable(&Disassembler))
                {
                    char CurrentByte = GetNextByte(&Disassembler);
                    if(NoDisassemblerError(&Disassembler))
                    {
                        string Instruction = DetermineInstruction(&Disassembler, CurrentByte);
                        PrintToConsole(&Instruction);
                    }
                }
            }
        }
        else
        {
            fprintf(stderr, "ERROR: Cannot allocate space for buffer. \n");
        }
        
        fclose(File);
    }
    else
    {
        fprintf(stderr, "ERROR: Cannot open file.");
    }
	
    return(0);
}