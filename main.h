#define function static
#define ArrayCount(Arr) sizeof(Arr) / sizeof(Arr[0])

typedef unsigned char uchar;
typedef size_t mmsz;
typedef uint32_t u32;
typedef int32_t s32;
typedef uint32_t b32;

static char *RegTable[] = 
{
    "AL", "CL", "DL", "BL",
    "AH", "CH", "DH", "BH",
    "AX", "CX", "DX", "BX",
    "SP", "BP", "SI", "DI"
};

struct string
{
    u32 Count;
    char *Data;
};

enum file_error
{
	FileError_None = 0,
    
	FileError_CannotOpenFile,
	FileError_SystemError,
	FileError_InsufficientBytesRead,
    FileError_Count
};

enum opcode : uchar
{
    OpCode_None = 0,
    
    OpCode_MOV_RegMemToRegMem = 0b100010,
    OpCode_MOV_ImmediateToRegMem = 0b1100011,
    OpCode_MOV_ImmediateToRegister = 0b1011,
    OpCode_MOV_MemToAccum = 0b1010000,
    OpCode_MOV_AccumToMem = 0b1010001,
    OpCode_MOV_RegMemToSegmentRegister = 0b10001110,
    OpCode_MOV_SegmentRegisterToRegMem = 0b10001100,
    
};

enum disassembler_error
{
    Disassembler_NoError = 0,
    
    Disassembler_NoBytesAvailable,
};

struct disassembler
{
    string Source;
    char *At;
    char *End;
    
    disassembler_error Error;
};

enum mode_encoding
{
    Mode_Memory_NoDisplacement = 0b00,
    Mode_Memory_8BitDisplacement = 0b01,
    Mode_Memory_16BitDisplacement = 0b10,
    Mode_RegisterToRegister = 0b11
};

enum displacement_type
{
    DisplacementType_None = 0,
    
};

enum field_encoding
{
    FieldEncoding_None = 0,
    
    FieldEncoding_Mod,
    FieldEncoding_Reg,
    FieldEncoding_Rm,
};

struct field
{
    uchar Value;
    field_encoding Encoding;
};

struct operation
{
    opcode Code;
    b32 FullRegister;
    displacement_type DisplacementType;
    field Source;
    field Destination;
};

struct platform_file
{
    FILE *FileHandle;
    file_error ErrorStatus;
};

function mmsz
GetFileSize(FILE *File)
{
	fseek(File, 0, SEEK_END);
	mmsz FileSize = ftell(File);
	fseek(File, 0, SEEK_SET);
	return(FileSize);
}

function file_error
ReadFile(FILE *File, void *Destination, mmsz DestinationSize, mmsz *BytesRead = 0)
{
    file_error Result = {};
    
	if(!File)
	{
		Result  = FileError_CannotOpenFile;
	}
    
	if(DestinationSize == -1)
	{
        Result = FileError_SystemError;
	}
    
	mmsz ReadSize = DestinationSize;
	if(fread(Destination, 1, DestinationSize, File) != ReadSize)
	{
        Result = FileError_InsufficientBytesRead;
	}
    
	if(BytesRead)
	{
		*BytesRead = ReadSize;
	}
    
	return(Result);
}

function bool
NoFileError(file_error FileError)
{
    return(FileError == FileError_None);
}

function u32
StringLength(char *String)
{
    u32 Length = 0;
    
    char *At = String;
    if(String)
    {
        for(;;)
        {
            if(*At == '\0')
            {
                break;
            }
            
            ++Length;
            ++At;
        }
    }
    
    return(Length);
}


//function string
//GetString()
//{
//string Result = {};
//Result.Data = (char *)malloc(sizeof(char) * MAX_PATH);
//Result.Count = MAX_PATH;
//return(Result);
//}

function string
GetString(char *String)
{
    string Result = {};
    Result.Data = String;
    Result.Count = StringLength(String);
    return(Result);
}

function void
CopyString(string *Dest, mmsz DestSize, char *Src)
{
    assert(Dest);
    
    Dest->Count = (u32)DestSize;
    Dest->Data = (char *)malloc(DestSize);
    
    memcpy_s(Dest->Data, DestSize, Src, DestSize);
}

function void
PrintToConsole(string *Src)
{
    char OutputBuffer[256];
    sprintf(OutputBuffer, "Decoded instruction: %.*s \n", Src->Count, Src->Data);
    OutputDebugString(OutputBuffer);
}